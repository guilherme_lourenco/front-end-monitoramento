import { Component, OnInit } from '@angular/core';
import { PoMenuItem } from '@po-ui/ng-components';


@Component({
  selector: 'side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.css']
})
export class SideMenuComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  readonly menus: Array<PoMenuItem> = [
    { label: 'Monitoramento', link: '/Monitoramento',  icon: 'po-icon po-icon-video-call', shortLabel: 'Monitoramento' },
    { label: 'Gerenciamento', link: '/Gerenciamento',  icon: 'po-icon po-icon-user', shortLabel: 'Gerenciamento',
    subItems: [
        { label: 'Usuários', link: '/Usuarios',  icon: '', shortLabel: 'Usuários' },
        { label: 'Portarias', link: '/Portarias',  icon: '', shortLabel: 'Portarias' },
      ]
    },
    { label: 'Portarias', link: '/Portarias',  icon: 'po-icon po-icon-truck', shortLabel: 'Portarias' },
    { label: 'Remoção', link: '/Remocao',  icon: 'po-icon po-icon-clear-content', shortLabel: 'Remoção' },
  ];

}
