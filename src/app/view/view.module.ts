import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComponentModule } from './../component/component.module';

import { PoTemplatesModule } from '@po-ui/ng-templates';
import { PoGridModule, PoModule } from '@po-ui/ng-components';
import { PoTabsModule } from '@po-ui/ng-components';
import { PoTagModule } from '@po-ui/ng-components';

import { MonitoramentoComponent } from './monitoramento/monitoramento.component';
import { GerenciamentoComponent } from './gerenciamento/gerenciamento.component';
import { PortariasComponent } from './portarias/portarias.component';
import { RemocaoComponent } from './remocao/remocao.component';

import { ViewRoutingModule } from './view-routing.module';


@NgModule({
  declarations: [
    GerenciamentoComponent,
    MonitoramentoComponent,
    PortariasComponent,
    RemocaoComponent
  ],
  imports: [
    CommonModule,
    ComponentModule,
    PoTemplatesModule,
    PoGridModule,
    PoModule,
    PoTabsModule,
    ViewRoutingModule,
    PoTagModule
  ]
})
export class ViewModule { }
